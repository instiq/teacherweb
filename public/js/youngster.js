function redirectOnNull(){
	//if the user is coming from outside the website, he must go throught the welcome page
	console.log(window.referrer);
	console.log(window.location);

	if(window.referrer){
		console.log("referer: " + window.referrer);
	}

	// if(window.location.pathname == '/'){

	// }else if(!window.referrer){
	// 	window.location = '/';
	// }
}

//Function for switching between the dashboard nav items
function activatePane(){

	var element = $(event.target);
	var location = $(element).data('target');

	showLoading();

	window.location = location;
	
	// '''var dashboardNavItems = $('.dashboard-nav-item');
				// $(dashboardNavItems).each(function(){
				// 	if($(this).hasClass('active')){
				// 		$(this).removeClass('active');
				// 	}
				// });
			
				// $(element).addClass('active');
			
				// var allPanes = $('.item-content');
				// $(allPanes).each(function(){
				// 	if($(this).attr('id') != id){
				// 		if($(this).hasClass('active'))
				// 			$(this).removeClass('active');
				// 	}else{
				// 		$(this).addClass('active');
				// 	}
				// });'''

}


function resolveNavAppearance(){
  if($(window).scrollTop() >= $('.navbar').outerHeight()){
    if($('.scroll-top').hasClass('hide'))
        $('.scroll-top').removeClass('hide');

    	
    }else{
        if(! $('.scroll-top').hasClass('hide'))
          $('.scroll-top').addClass('hide');
    }  
}

function tryLoading(){
	//confirm we are on the welcome page 
	if(window.location.pathname == '/' || window.location.pathname == '/welcome'){

		//if we are on the welcome page, start drawing the loading status


		//get the loading bar div
		var loadingBar = $('.loading_bar')
		console.log(loadingBar);

		//get its width
		var loadingBarWidth = $(loadingBar).width();
		console.log(loadingBarWidth);

		//get the loading status div
		var loadingStatus = $('.loading_status')
		console.log(loadingStatus);

		//get its width
		var loadingStatusWidth = $(loadingStatus).width();
		console.log(loadingStatusWidth);

		//start a timer to increase the width to the loading status

		var timer = setInterval(function(){
		

			if(loadingStatusWidth < loadingBarWidth){

				loadingStatusWidth += loadingBarWidth / 15;
				$(loadingStatus).width(loadingStatusWidth + 'px');
		
			}else{

				clearInterval(timer);
				console.log('timer stopped');

				//go to the login page now
				window.location = '/login';
			}


		}, 200);

		//when the width of the loading staus equals that of the loading bar, rdirect to login page conditionally.
		
	}
}


function goToLogin(){
	window.location = '/login';
}


function showNextSection(index){
	//grap the second form section on the registrration page
	if(index == 2){

		var secondSection = $('#second_form_section');
		console.log(secondSection);


		//slide it left.
		$(secondSection).animate({left: '0px'}, 'slow');

	}else if(index == 3){
		var thirdSection = $('#third_form_section');
		console.log(thirdSection);


		//slide it left.
		$(thirdSection).animate({left: '0px'}, 'slow');
	}

	


}


$(document).ready(function(){

	redirectOnNull();

	resolveNavAppearance();
	tryLoading();
	
	//We hide the scoll to top div or show as needed
	  window.addEventListener('scroll', function(){
	    resolveNavAppearance();
	  });






	  //If the scroll-top div is clicked
	  $('.scroll-top').on('click', function(){
	    $('html,body').animate({scrollTop : 0}, 'fast');
	  });  

	  //On clicking the menu icon, display the popup
	  // $('#menu-icon').on('click', function(){
	  // 	if($('.dashboard-popup').css('display') == 'none'){
	  // 		$('.dashboard-popup').css('display', 'block');
	  // 	}else{
	  // 		$('.dashboard-popup').css('display', 'none');
	  // 	}
	  // });

	  // window.addEventListener('click', function(){
	  // 	if($('.dashboard-popup').css('display') == 'block'){
	  // 		console.log(event.target);
	  // 		$('.dashboard-popup').css('display', 'none');
	  // 	}
	  	
	  // });


	  //If we are on the dashboard...do this
	  var dashboardNavItems = $('.dashboard-nav-item');
	  if(dashboardNavItems){
	  	console.log('Dashboard Nav Items: ')
	  	console.log($(dashboardNavItems));
		  $(dashboardNavItems).each(function(){
			$(this).on('click', function(){
				console.log($(this).data('target'));
				activatePane();
			});
		});
	  } 

	  $('.broadcast-item').on('click', function(){
          var broadcastList = $('.broadcast-item');
          //$('.broadcast-details').fadeIn('fast');
          //Instead we expand the clicked one
          //First we return all to normal size
          for(var i = 0; i < $(broadcastList).length; i++){
              if($(broadcastList[i]).hasClass('active')){
                  $(broadcastList[i]).removeClass('active');
                  $(broadcastList[i]).find($('.broadcast-excerpt')).css('display', 'block');
                  $(broadcastList[i]).find($('.broadcast-message')).css('display', 'none');
              
              }
              $(broadcastList[i]).css('opacity', '.5');
          }
          //Then we add the class that expands them
          $(this).addClass('active');
          $(this).find($('.broadcast-excerpt')).css('display', 'none');
          $(this).find($('.broadcast-message')).css('display', 'block');
           $(this).css('opacity', '1');
          
          //$('.broadcast-list').fadeOut('fast');
      });


	  //When on the digital homework pane, the back-to-set-assignment botton should get ths listener
	  $('#back-to-set-assignment').on('click', function(){
	  	$('#homework-history-subpane').css('display', 'none');
	  });

	  //We use this to show the homework history
	  $('#teacher-view-homework-history').on('click', function(){
	  	$('#homework-history-subpane').css('display', 'block');
	  });
      
});

//function to show loading
function showLoading(){
	$(".loading-div").fadeIn('fast');
}

//function to hide loading
function hideLoading(){
	$(".loading-div").fadeOut('fast');
}

function launchConfirmationModal(message, action){
	$('#confirmation-modal').find($('.modal-body')).html(`<p>${message}</p>`);
	$('#confirmation-modal').find($('#positive')).click(function(){action()});
	$('#confirmation-modal').modal('show');
}

function launchInfoModal(message){
	$('#info-modal').find($('.modal-body')).html(`<p>${message}</p>`);
	$('#info-modal').modal('show');
}