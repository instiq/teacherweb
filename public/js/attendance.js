

$(document).ready(function(){
	//We ensure something happens when a teacher clicks a student
	$('.child-attendance-item').on('click', function(){
		var image = $(this).find($("img[class='attendance-status']"));
		var input = $(this).find($("input[name='attendance-status']"));

		if($(image).attr('src') == '/images/icons/check.png'){
			$(image).attr('src', '/images/icons/uncheck.png');
			$(input).val('absent');
		}else{
			$(image).attr('src', '/images/icons/check.png');
			$(input).val('present');
		}
		
	});


	//We set a listener on the submit attendance button
	$('#update-attendance').click(function(){
		confirmSubmitAttendance();
	});
});

function confirmSubmitAttendance(){

	launchConfirmationModal('Are you sure you want to submit the attendance?', submitAttendance);

}

function submitAttendance(){
	
	$('#confirmation-modal').modal('hide');
	//TODO
	//Grab all attendance status inputs 
	var attendanceInputs = $('.attendance-status-input');

	if(attendanceInputs.length > 0){

		var requestArray = [];

		console.log('attendances', attendanceInputs);

		for(var i = 0; i < attendanceInputs.length; i++){
			var childAttendanceJSON = {}
			childAttendanceJSON['studentId'] = $(attendanceInputs[i]).attr('id');
			childAttendanceJSON['status'] = $(attendanceInputs[i]).val() == "" ? 'absent' : $(attendanceInputs[i]).val();
			requestArray.push(childAttendanceJSON);
		}

		console.log('requestArray', requestArray)

		//Now axios this motherfucker!!!!
		showLoading();
		axios.post('/submit-attendance', {'students' : requestArray})
		.then(function(response){
			hideLoading();
			if(response.data.status == 200){
				launchInfoModal('Attendance saved successfully');
			}else{
				console.log(response.data);
			}
			//alert(response.data.status);
			//console.log(response.data.status)
		})
		.catch(function(error){
			hideLoading();
			console.log(error)
		});

	}else{
		launchInfoModal('Nothing to submit');
	}

	//alert('Submitting...');

}