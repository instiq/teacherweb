<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Curl;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function tryLogin(Request $request)
    {


        $email = $request->email;
        $password = $request->password;

        try{
            // return response()->json(['status' => 'submitted', 'email' => $email, 'password' => $password], 200);

            $_response = Curl::to( config("youngster_teacher.login_route") )
            ->withData([
                'type' => config("youngster_teacher.teacher_type"),
                'email' => $email,
                'password' => $password                
            ])
            ->withResponseHeaders()
            ->returnResponseObject()
            ->asJson()
            ->post();
   

           $response_collection = collect($_response);
           $response_status = $response_collection['status'];
           $response_content = $response_collection['content'];
           $response_mssg = $response_content->msg;           
           
           //return collect($_response);
           //return $response_mssg;

           if($response_mssg == 'Invalid email or password.'){

                session()->flash('failed_login', $response_mssg);
                return back();

           }else if($response_status == 200){

                $response_token = collect($_response)['headers'][config('youngster_teacher.token_name')];

                $id = $response_content->data->id;
                $first_name = $response_content->data->firstName;
                $last_name = $response_content->data->lastName;
                $img = $response_content->data->img;
                 
                $school_id = $response_content->data->schoolId;
                $class_id = $response_content->data->classId;
                $class_name = $response_content->data->className;
                $school_name = $response_content->data->schoolName;
                $school_address = $response_content->data->schoolAddress;
                
                
                $request->session()->regenerate();


                session(['x-auth-token' => $response_token]);
                session(['id' => $id]);
                session(['first_name' => $first_name]);
                session(['last_name' => $last_name]);
                session(['img' => $img]);
                session(['class_id' => $class_id]);
                session(['class_name' => $class_name]);
                session(['email' => $email]);


                session(['school_id' => $school_id]);
                session(['school_name' => $school_name]);
                session(['school_address' => $school_address]);


                
                

                // return 'session: ' . session('id') . session('first_name') . session('last_name') . session('img') . session('class_id') . session('class_name') . session('school_id').session('school_name') . session('school_address');

                return redirect()->intended($this->redirectPath());

           }



            //return response()->json(['content' => $_response->content, 'headers' => $_response->headers]);

            return $response_mssg;

            //return $_response->toString();

        }catch(\Exception $e){
            //return response()->json($e->getMessage());
            session()->flash('failed_login', 'Login failed. Please check your internet connection');
                return back();
            //return response()->json($e->getMessage());
        }
    }

    public function tryLogout(Request $request)
    {

        $request->session()->regenerate();

        session()->forget('x-auth-token');
        session()->forget('school_id');
        session()->forget('school_logo');

        //return 'token: ' . session('school_name');

        return redirect('login');

    }
}
