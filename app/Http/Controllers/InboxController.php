<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class InboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //here we fetch or the messages exchanged between the parent and the school

        $_response = Curl::to( config("youngster_teacher.inbox") . session('schoolId') )
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->get();

         if($_response){

            if(collect($_response)['status'] == 200){

                $inbox = collect($_response)['content'];

                return view('dashboard')->with(['inbox' => $inbox]);
            }

         }else{
            try{

                return collect(collect($_response)['content'])['msg'];

            }catch(\Exception $e){

                return collect(['msg' => 'Error retrieving inbox']);

            }
         }

         return view('dashboard')->with(['inbox' => [] ]);
         //return collect();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        //$schoolId = $request->schoolId;
        $headline = $request->headline;
        $body = $request->body;

        //return $request->all();

        $_response = Curl::to( config("youngster_teacher.send_message") )
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->withData(['headline' => $headline, 'body' => $body])
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->post();

        if($_response){

            $status = collect($_response)['status'];

            //return collect($_response);

            if($status == 200){

                session()->flash('success_submit', 'Message sent successfully');

                return back();

            }else{

                session()->flash('failed_submit', 'Error sending message. Please try again later.');

                return back();


            }

        }else{
            session()->flash('failed_submit', 'Error sending Message :(');

                return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
