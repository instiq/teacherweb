<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $_response = Curl::to( config("youngster_teacher.get_students_parents")  )
                   ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                   ->withResponseHeaders()
                   ->returnResponseObject()
                   ->asJson()
                   ->get();

        $_response2 = Curl::to( config("youngster_teacher.get_all_chats")  )
                   ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                   ->withResponseHeaders()
                   ->returnResponseObject()
                   ->asJson()
                   ->get();


        if($_response && $_response2){

            $status = collect($_response)['status'];
            $status2 = collect($_response2)['status'];

            if($status == 200 && $status2 == 200){

                $parent = collect($_response)['content'];
                $chats = collect($_response2)['content'];

                return view('dashboard')->with(['parent'=> $parent, 'chats'=> $chats]);

            }else{
                return 'There was an error getting chats';
            }
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendMessage(Request $request)
    {
        //

        //return $request->all();

        $parentId = $request->parentId;
        $body = $request->body;
        $_response = Curl::to( config("youngster_teacher.send_chat_message") )
                   ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                   ->withdata(['parentId' => $parentId, 'body' => $body])
                   ->withResponseHeaders()
                   ->returnResponseObject()
                   ->asJson()
                   ->post();
        if($_response){
           $status = collect($_response)['status'];
           //return collect($_response);
           return $status;
        }
    }


    public function getChatMessages(Request $request){
       $chatId = $request->chatId;
       $page = $request->page;
       $_response = Curl::to( config("youngster_teacher.get_chat_messages") . $chatId . '/' . $page )
                   ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                   ->withResponseHeaders()
                   ->returnResponseObject()
                   ->asJson()
                   ->get();
       if($_response){
           $status = collect($_response)['status'];
           return collect($_response);
           // if($status == 200){
           //     return collect($_response)['content'];
           // }else{
           //     return 'Could not get chats';
           // }
       }else{
           return 'Error getting chats';
       }
   }
}
