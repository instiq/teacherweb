<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //we use this to get all assignments
        $_response =  Curl::to( config('youngster_teacher.assignment'))
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->get();

        if($_response){

             $status = collect($_response)['status'];
             
             if($status == 200){

                $assignments = collect(collect($_response)['content'])['assignment'];


                return view('dashboard')->with(['assignments' => $assignments]);
             
             }else{
                //return 'Error getting assignments';
                return view('dashboard')->with(['assignments' => 'Error getting assignments' ]);
             }

        }else{
            return view('dashboard')->with(['assignments' => 'Error getting assignments' ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $title = $request->title;
        $description = $request->description;
        $date = $request->deadlineDate;
        $time = $request->deadlineTime;

        $_response =  Curl::to( config('youngster_teacher.assignment'))
                    ->withResponseHeaders()
                    ->withData(['title' => $title, 'description' => $description, 'deadline' => $date . 'T' . $time . ':00.000Z',])
                    ->returnResponseObject()
                    ->asJson()
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->post();

        if($_response){

             $status = collect($_response)['status'];
             
             if($status == 200){

                session()->flash('success_submit', 'Assignment sent successfully...');

                return back();
             
             }else{
                session()->flash('failed_submit', 'Assignment could not be sent :(');

                return back();
             }

        }else{
            session()->flash('failed_submit', 'Error sending assignment :(');

            return back();
        }
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
