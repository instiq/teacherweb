<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class DashboardController extends Controller
{
    public function __construct(){
        //$this->middleware('auth');
    }

    public function index()
    {
        //
        $_response2 = Curl::to( config("youngster_teacher.get_broadcasts") . session('school_id') )
                   ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                   ->withResponseHeaders()
                   ->returnResponseObject()
                   ->asJson()
                   ->get();
       if ($_response2){
            $status = collect($_response2)['status'];

            if($status == 200) { 

                return view('dashboard')->with(['broadcasts'=> collect($_response2)['content']]);

            } else{
                return view('dashboard')->with(['msg'=> collect($_response2)]); //refine this
            }

       }else
            return view('dashboard')->with(['msg'=> 'Error retrieving broadcasts']);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
