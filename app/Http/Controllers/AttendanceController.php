<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //we use this to get all students
        $_response =  Curl::to( config('youngster_teacher.get_students'))
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->get();

        if($_response){

             $status = collect($_response)['status'];
             
             if($status == 200){

                $students = collect($_response)['content'];


                return view('dashboard')->with(['students' => $students]);
             
             }else{
                return 'Error getting students';
             }


        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Here we send the attendance
        //return $request->all();

         $_response =  Curl::to( config('youngster_teacher.submit_attendance'))
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->withData(["students" => $request->students] )
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->post();

        if($_response){

            return collect($_response);

        }else{
            return 'nothing oh!';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
