<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class TeacherStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $_response =  Curl::to( config('youngster_teacher.get_status') )
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->get();

        if($_response){

             $status = collect($_response)['status'];

             //return collect($_response);
         

              if($status == 200){

                $status = collect($_response)['content'];

                //return $status;

                return view('dashboard')->with(['status' => collect($status) ]);
              }
        }
             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        //
         
        
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        $status = $request->status;

        $_response = Curl::to( config("youngster_teacher.set_status") )
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->withData(['status' => $status])
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->post();

        return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
