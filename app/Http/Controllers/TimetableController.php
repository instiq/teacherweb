<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class TimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
         $_response =  Curl::to( config('youngster_teacher.get_timetable'))
                    ->withResponseHeaders()
                    ->returnResponseObject()
                    ->asJson()
                    ->withData(["classId" => session('class_id'), "schoolId" => session('school_id')] )
                    ->withHeaders(['x-auth-token: ' . session('x-auth-token')])
                    ->post();
        if($_response){
            //return collect($_response);//collect($_response)['content'];
            $status = collect($_response)['status'];
            if($status == 200){
                $timetable = collect(collect(collect($_response)['content'])['table']);
                return view('dashboard')->with(['timetable' => $timetable]);
            }else{
                return view('dashboard')->with(['timetable' => []]);
            }
            
        }else{
            return 'No timetable to show';
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
