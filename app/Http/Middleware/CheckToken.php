<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Route;
class CheckToken
{
   /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
   public function handle($request, Closure $next)
   {
       if($request->hasSession()){
           if(!Route::is('login') && ! $request->session()->has(config('youngster_teacher.token_name')) )
               return redirect('login');
       }else{
           return redirect('login');
       }
       return $next($request);
   }
}