<?php


$base_url = 'http://api.myoungster.com/api/';
$app_url = 'http://api.myoungster.com/';

return [

	'teacher_type' => 'teacher',

	'token_name' => 'x-auth-token',
	'school_id' => 'school_id',
	'image_prefix' => $app_url . 'public/images/',

	//This is the school login route 
	'login_route' => $base_url . 'auth/driver-teacher',
	 //get timetable
    'get_timetable' => $base_url . 'school/time-table/read',

    'get_students' => $base_url . 'teacher/get-students',

	//get all broadcasts
	'get_broadcasts' => $base_url . 'school/broadcast/', // append school id to get all broadcasts

	//submit attendance
	'submit_attendance' => $base_url . 'teacher/attendance', // append school id to get all broadcasts

	//handle asignments. post and get assignments
	'assignment' => $base_url . 'teacher/assignment',
	

	 'get_status' => $base_url . 'teacher/availability',

	 'set_status' => $base_url . 'teacher/toggle-availability',

	 'inbox' => $base_url . 'teacher/get-school/message',

	 'send_message' => $base_url . 'teacher/send-school/message',


	 'get_students_parents' => $base_url . 'teacher/student/parents',

	 'get_all_chats' => $base_url . 'teacher/get/all/chats',

	 'send_chat_message' => $base_url . 'teacher/send/chat',

	 'get_chat_messages' => $base_url . 'teacher/get/chat/', //append chat id and page number
];