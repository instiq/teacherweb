@extends('partials.master')

@section('title')
Welcome
@endsection 

@section('navigation')

@endsection

@section('content')

<div class="welcome">
    <div id="welcome_logo_div">
        <img src="/images/logo.png" alt="welcome logo" id="welcome_logo" />
        <div id="side_div">
            <h2>Youngster</h2>
            <p>Parent | Educator | Pupil</p>
        </div>
    </div>

    <div class="loading_bar well" style="margin-top: 200px;">
        <div class="loading_status"></div>
    </div>

</div>



@endsection

@section('footer')

@endsection