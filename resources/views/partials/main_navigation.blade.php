<nav class="navbar">
  <a class="navbar-brand" href="/dashboard">
    <img id="nav_logo" src="/images/nav_logo.png" alt="nav logo">    
  </a>

  @include('partials.login_or_register')
</nav>