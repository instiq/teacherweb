<div class="item-content row" id="subject-time-table-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane">
						
			<div id="time-table-header">
				<h4>Time Table</h4>
				<p>{{ session('class_name') }}</p>
				<img id="penguin-image" src="/images/icons/penguin.png" alt="penguin image" />
			</div>

			<div id="time-table-div">
                @if($timetable ==  [])
                    <div style="height: 200px; padding-top: 100px; text-align: center; font-weight: bold; color: #bbb; font-size: 20px;">
                        No current timetable
                    </div>
                @else
                    <table class="table timetable current">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          @foreach(collect(($timetable[0])->data) as $a_time)
                          <th scope="col">
                            {!! $a_time->startTime !!} - {!! $a_time->endTime !!}
                          </th>
                          @endforeach
                        </tr>
                      </thead>
                      <tbody>
                        @foreach(collect($timetable) as $table_row)
                        <tr>
                          <th scope="row">{!! $table_row->dayOfTheWeek !!}</th>
                          @foreach(collect($table_row->data) as $a_time)
                          <td>
                            {!! $a_time->subject !!}
                          </td>
                          @endforeach                                 
                        </tr>
                        @endforeach                            
                      </tbody>
                    </table>
                @endif  
		</div>
	</div>
	
	@include('partials.dashboard.school_sidebar')
	
</div>