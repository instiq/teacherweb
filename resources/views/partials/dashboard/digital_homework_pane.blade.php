<div class="item-content row" id="digital-homework-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane" style="overflow-y: none;">

		@include('partials.flash')

		<div class="pane row" >
						
			<div class="col-md-5 col-lg-5 col-sm-5 homework-recipient-list">
				<div id="homework-history-subpane">

					<h3 class="text-center text-white">Homework History</h3>
					<!-- {{ collect($assignments) }} -->
					<div id="teacher-homework-history" style="overflow-y: scroll; height: 450px;">

						@if(count(collect($assignments)) > 0)
							@foreach(collect($assignments) as $assignment)
								<div class="homework-list-item">
									<div class="school-image">
										<img  src="/images/school/school.png" alt="School image" />
									</div>
									<div class="homework-details">
										<h6 class="subject-title">{{ $assignment->title }}</h6>
										<p class="assignment-header">{{ substr($assignment->description, 0, 25) . '...' }}</p>
										<small class="time-set tiny">{{ substr($assignment->deadline, 0, 10) }}</small>
										<small class="time-left tiny">{{ substr($assignment->deadline, 11, 5) }}</small>
									</div>
								</div>

								<input type="hidden" id="assignment-title" value="{{ $assignment->title }}" />
								<input type="hidden" id="assignment-description" value="{{ $assignment->description }}" />
								<input type="hidden" id="assignment-deadline-date" value="{{ substr($assignment->deadline, 0, 10) }}" />
								<input type="hidden" id="assignment-deadline-time" value="{{ substr($assignment->deadline, 11, 5) }}" />
							@endforeach
						@else

						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Agriculture Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Mathematics Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>

						@endif

					</div>

				</div>
				
			</div>

			<div class="col-md-7 col-lg-7 col-sm-7 homework-detail">

				
				<div id="homework">
					<form action="{{ route('send_assignment_route') }}" method="POST" 
					id="send-assignment-form" enctype="multipart/form-data" onsubmit="showLoading();">

						@csrf

						<div id="set-homework-top" class="row" style="align-items: center;">

							<div class="col-md-3"><b>Deadline: </b></div>
							<div class="col-md-5" id="assignment-date-div">
								<label for="set-assignment-date" id="set-deadline">Date</label>
								<input type="date" class="set-assignment-date forms-ext" name="deadlineDate" id="set-assignment-date" required />
							</div>

							<div class="col-md-4" id="assignment-time-div">
								<label for="set-assignment-time" id="set-deadline">Time</label>
								<input type="time" class="set-assignment-time forms-ext" name="deadlineTime" id="set-assignment-time" required/>
							</div>
							

						</div>
						<input type="text" class="form-control" id="homework-subject" name="title" placeholder="Subject Title" required>

						<textarea class="form-control" id="homework-desc" name="description" rows="6" style="margin: 20px 0px; text-align: left;" placeholder="Description here..." required>
							
						</textarea> 

						<input type="submit" class="btn btn-primary" style="width: 100%; margin-top: 20px;" 
						value="{{ __('Send') }}" >

					</form>

					<!-- <img id="send-assignment-image" src="/images/icons/send.png" alt="" /> -->
				</div>
			</div>
		</div>
	</div>
	
	@include('partials.dashboard.school_sidebar')

</div>