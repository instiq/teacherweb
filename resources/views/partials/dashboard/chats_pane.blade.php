<!-- {{collect($chats)}} -->
<div class="item-content row" id="chats-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane row" >
						
			<div class="col-md-5 col-lg-5 select-chat">
				<h5 class="header" >Parents</h5>
				@foreach(collect($parent) as $parent)
				<div class="chat-item">
					<img src="{{ config('youngster_teacher.image_prefix') . $parent->img }}" style="width: 50px; clip-path: circle(25px at 25px 25px); "/>
					
					<div class="last-message">
						<h6>{{ $parent->firstName }} {{ $parent->lastName }}</h6>
						<p>Parent: @if(count($parent->parents) > 0)
										{{ $parent->parents[0]->firstName }} {{ $parent->parents[0]->lastName }}

									@else

										{{ 'Not linked' }}

									@endif							
							
						</p>
					</div>
					<div class="next">
						<i class="fas fa-chevron-right"></i>
					</div>

					<input type="hidden" id="{{ $parent->_id }}-parentId" value="@if(count($parent->parents) > 0){{$parent->parents[0]->_id }}@else{{ '' }}@endif">

					<input type="hidden" id="{{ $parent->_id }}-parentImg" value="@if(count($parent->parents) > 0){{ $parent->parents[0]->img }}@else{{ '' }}@endif">

					<input type="hidden" id="{{ $parent->_id }}-parentName" value="@if(count($parent->parents) > 0){{ $parent->parents[0]->firstName }} {{ $parent->parents[0]->lastName }}@else{{ '' }}@endif">
					
					<input type="hidden" data-parent-id="@if(count($parent->parents) > 0){{ $parent->parents[0]->_id }}@else{{ '' }}@endif" id="{{ $parent->_id }}-chatId" value="" />
					

				</div>
				
				@endforeach
			</div>
			<div class="col-md-7 col-lg-7 chat ">
				<h5 class="header" ></h5>
				<div id="messages-container">
					

				</div>

				<div id="enter-message" class="row">

					<input type="text" class="form-control" id="enter-message-input" placeholder="Type message">
					<img id="send-message-image" src="/images/icons/send.png" alt="" />
					
				</div>


			</div>

		</div>
	</div>

	@include('partials.dashboard.school_sidebar')
	
</div>

<script type="text/javascript">

	var chatsArray = [];

	var parentId = '';
	var parentName = '';
	var parentImg = '';
	var chatId = '';

	var messagesContainer = $('#messages-container');

	function setChatIds(){

		@php
			if(count($chats) > 0){
				//some shit should happen here
				for($i = 0; $i < count($chats); $i++){
					echo 'chatsArray.push({"parentId" : "' . $chats[$i]->parentId->_id . '", "chatId" : "' . $chats[$i]->_id . '"});';
				}
			}
			
		@endphp

		console.log('chatsArray', chatsArray);

		//Grab all hidden inputs that end with chat id so as to set their chat ids
		var hiddenChatIds = $('input[id$="chatId"]');

		console.log('hiddenChatIds', hiddenChatIds.length);

		//now loop through them and set their chatId values
		for(var i = 0; i < hiddenChatIds.length; i++){
			for(var j = 0; j < chatsArray.length; j++){
				console.log("$(hiddenChatIds[i]).data('parentId')", $(hiddenChatIds[i]).data('parentId'));
				console.log("chatsArray[j]['parentId']", chatsArray[j]['parentId']);

				if($(hiddenChatIds[i]).data('parentId') == chatsArray[j]['parentId']){
					$(hiddenChatIds[i]).val(chatsArray[j]['chatId']);
					break;
				}
			}
		} 

	}

	$(document).ready(function(){

		//First, we set chat ids
		setChatIds();

		$('.chat-item').on('click', function(){

			if(! $(this).hasClass('active')){
				$('.chat-item').removeClass('active');
				$(this).addClass('active');
			}	
			//clear the message input
			$('#enter-message-input').val('');	
			//We make an ajax request here to get the chat

			//First we get the chat id
			chatId = $(this).find($('input[id$="chatId"]')).val();

			//console.log('chatId', chatId );

			//we set the parent id here
			parentId = $(this).find($('input[id$="parentId"]')).val();

			//we set the receiver's name
			parentName = $(this).find($('input[id$="parentName"]')).val();
			$('.chat .header').text(parentName);


			if(chatId == ''){
				//clear the messages part wait for teacher to send messages
				$('#messages-container').html('');
			}else{
				//make a request to get previous chat messages using the chat id
				showLoading();
				getPreviousChat(1);
				hideLoading();
			}

		});

		//Enter message input is bound to a listener here
		$('#enter-message-input').keyup(function(event){
			var code = event.keycode || event.which;
			if(code == 13){
				sendMessage();
			}
		});

		//Send message image is bound to a listener here
		$('#send-message-image').on('click', function(){			
			sendMessage();
		});

		//we check for messages periodicaly
		setInterval(function(){

			if(chatId != ''){
				getPreviousChat(1);
			}
			
		}, 3000)

	});


	function getPreviousChat(page){
		axios.post('/get-chat-messages', {'chatId' : chatId, 'page' : page})
		.then(function(response){

			console.log(response.data.content);
			chatsArray = response.data.content.chat;
			displayPreviousChats(parentName);

		}).catch(function(error){
			console.log(error);
			alert('Please check your internet connection');
		});
	}

	function sendMessage(){
		//We check if #enter-message-input is not empty then send the message
		var mssg = $('#enter-message-input').val().trim();
		if(mssg != ''){
			//alert('We are sending the message...' + mssg);
			axios.post('/send-chat-message', {'parentId' : parentId, 'body' : mssg})
			.then(function(response){
				console.log(response.data);
				if(response.data == 200){
				//create a sent mail and append to the messages-container
                    var newMessage = `<p class="message-time">${new Date().toString().substring(0, 22)}</p>
                    <div class="out-message">
                        <div class="initials">
                            <p>Me</p>
                        </div>
                        <div class="message-body">
                            <p>${mssg}</p>
                        </div>
                    </div>`;
                    $(messagesContainer).append(newMessage); //append the new message
                    $(messagesContainer).animate({scrollTop: 10000000000 }, 'slow'); //and scroll down
                    //Then we clear the message input
                    $('#enter-message-input').val('');
                }else{
                    alert('Could not send message');
                    console.log('sending report', response.data);
                }
			}).catch(function(){
				console.log(error);
			});
		}

	}

	function displayPreviousChats(parentName){
        
        var html = '';
        var isOutMessage = true;
        var parentNameArray = parentName.split(' ');
        var parentInitials = parentNameArray[0].charAt(0) + parentNameArray[1].charAt(0)
        for(var i = 0; i < chatsArray.length; i++){
            if(chatsArray[i].parentId){
                isOutMessage = false;
            }else{
                isOutMessage = true;
            }
            html += `<p class="message-time">${chatsArray[i].createdAt}</p>
                    <div class="${isOutMessage ? 'out-message' : 'in-message ml-auto'}">
                        <div class="initials">
                            <p>${isOutMessage ? 'Me' : parentInitials}</p>
                        </div>
                        <div class="message-body">
                            <p>${chatsArray[i].body}</p>
                        </div>
                    </div>`;
            
            
        }
        hideLoading();
        
        $(messagesContainer).html(html);
        $(messagesContainer).animate({scrollTop: 10000000000 }, 'slow');
    }

	

</script>