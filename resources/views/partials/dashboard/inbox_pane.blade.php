

<div class="item-content row" id="inbox-pane">
	<!-- {{ collect($inbox) }} -->
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div style="text-align: center; padding-top: 20px;">
			@include('partials.flash')
		</div>
		
		<div class="pane" style="position: relative;" >

			<ul class="nav nav-tabs" id="messagesTab" role="tablist">

			  <li class="nav-item">
			    <a class="nav-link active" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="true">Message</a>
			  </li>

			  <!-- <li class="nav-item">
			    <a class="nav-link" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Notifications</a>
			  </li> -->
			  
			</ul>
			<div class="tab-content" id="tabContent">
			  
			  <div class="tab-pane fade show active" id="messages" role="tabpanel" aria-labelledby="messages-tab">

			  	@if(count(collect($inbox)) > 0)

				  	@foreach(collect($inbox) as $inbox_mssg)
					  	<div  class="single-message row">
					  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
					  			<input class="form-check-input" type="checkbox" value="" id="select-message">
					  			<img src="/images/icons/star_selected.png" alt="importance image" />
					  		</div>
					  		
					  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
					  			<p>@if($inbox_mssg->type == 'toSchool') Me @else School Admin @endif</p>
					  		</div>
					  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
					  			<p>{{ $inbox_mssg->headline }}</p>
					  		</div>
					  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
					  			<p>{{ $inbox_mssg->body }}</p>
					  		</div>
					  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
					  			<i class="fas fa-trash"></i>
					  		</div>
					  	</div>
				  	@endforeach

				@else

					<div  class="single-message row">
				  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
				  			<input class="form-check-input" type="checkbox" value="" id="select-message">
				  			<img src="/images/icons/star_selected.png" alt="importance image" />
				  		</div>
				  		
				  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
				  			<p>Sample school</p>
				  		</div>
				  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
				  			<p>New Assembly Time</p>
				  		</div>
				  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
				  			<p>As we previously stated, every student must report to the assembly ground before the wake up</p>
				  		</div>
				  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
				  			<i class="fas fa-trash"></i>
				  		</div>
				  	</div>
				  	<div  class="single-message row">
				  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
				  			<input class="form-check-input" type="checkbox" value="" id="select-message">
				  			<img src="/images/icons/star_selected.png" alt="importance image" />
				  		</div>
				  		
				  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
				  			<p>Me</p>
				  		</div>
				  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
				  			<p>Sample Parent</p>
				  		</div>
				  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
				  			<p>This is a sample message being sent by parent</p>
				  		</div>
				  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
				  			<i class="fas fa-trash"></i>
				  		</div>
				  	</div>


				@endif

			  	
			  </div>

			  <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
			  	<p>Notifications tab</p>
			  </div>

			</div> <!-- end of tab content -->

			

			<div class="compose-message" style="position: absolute; width: 80%; min-width: 300px; margin-left: 50%; bottom: 100px; transform: translate(-50%); background-color: white; box-shadow: 0px 0px 5px rgba(0,0,0,.5); padding: 20px; border-radius: 16px; display: none;" >

				<i class="fas fa-long-arrow-alt-left back_image" style="cursor: pointer;"></i>
				
				<form action="{{ route('send_message_route') }}" method="POST" id="compose_message_form">
					
					@csrf
				  
				  <div class="form-group">
				    <input type="text" class="form-control" id="message-title" aria-describedby="messageTitle" name="headline" placeholder="Title">			    
				  </div>
				  
				  <textarea class="form-control" id="message-back-input" rows="3" name="body" placeholder="Content here..."></textarea>

				  <input type="hidden" name="parentId" value="{{ session('parent_id') }}">
				  <input type="hidden" name="schoolId" value="{{ session('schoolId') }}">

			  
				</form>

				<div class="actions">
					<img src="/images/icons/send.png" id="send-the-message" alt="" style="width: 50px;  float: right; margin-right: 16px;" onclick="$('.compose-message').fadeOut('fast'); showLoading(); $('#compose_message_form').submit();"/>
				</div>

			</div>

			<img src="/images/icons/write.png" class="action-create" alt="" style="position: absolute; width: 50px; bottom: 50px; right: 20px; cursor: pointer; " onclick="toggleComposeMessage(); " />


		</div> <!-- end of pane -->

		

	</div> <!-- end of content pane -->

	@include('partials.dashboard.school_sidebar')

	<script type="text/javascript">
		function toggleComposeMessage(){
			var composeDisplay = $('.compose-message').css('display');

			if(composeDisplay == 'block')
				$('.compose-message').css('display', 'none');
			else
				$('.compose-message').css('display', 'block');
		}
	</script>
	
</div>