<!-- {{ collect ($broadcasts)}} -->
<div class="item-content row active" id="broadcast-pane" >
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">

		<div class="pane" >
			<div style="position: relative; text-align: center; height: 50px;">
				<div class="divider"></div>
				<p style="background-color: white; display: inline-block; padding: 0px 0px;">Today</p>
			</div>
		    @foreach(collect($broadcasts)->reverse() as $broadcast) 

			<div class="broadcast-item @if($broadcast->flag == 'important') important @endif">
				<h6>{{ $broadcast->headline }}</h6>
				<p class="broadcast-message">{!! $broadcast->body !!}</p>
                <p class="broadcast-excerpt">{!! substr($broadcast->body, 0, 35) . '...' !!}</p>
			</div>	
			@endforeach				

			
			
			
		</div>

	</div>

	@include('partials.dashboard.school_sidebar')
</div>