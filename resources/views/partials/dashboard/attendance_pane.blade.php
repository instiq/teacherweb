@include('partials.modals.confirmation-modal')
@include('partials.modals.info-modal')
<div class="item-content row" id="attendance-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane" >
			<div id="attendance-date">{{ date('M d, Y') }}</div>
			<!-- {{ collect($students) }} -->
			<div id="child-attendance" class="row">

				<div class="child-attendance-column row">
					@foreach(collect($students) as $student)
					<div class="child-attendance-item col-md-6">
						<img class="child-image" src="{{ config('youngster_teacher.image_prefix') . $student->img}}" alt="child image" />
						<p class="child-name">{{ $student->firstName }}  {{ $student->otherName }} {{ $student->lastName }}</p>
						<img class="attendance-status" id="{{ $student->_id }}-indicator"
							@php 
								if(isset($student->attendance)){
								 	if($student->attendance->status == 'present'){ 
								 		echo 'src="/images/icons/check.png"'; 
								 	}else{
								 		echo 'src="/images/icons/uncheck.png"'; 
								 	} 
								 }else{ 
								 	echo 'src="/images/icons/uncheck.png"';
								 } 
							@endphp" 

						 alt="" 

						 />
						<input type="hidden" name="attendance-status" class="attendance-status-input" id="{{  $student->_id }}"  
						@php 
							if(isset($student->attendance)) 
								echo 'value="'.$student->attendance->status.'"'; 
							else 
								echo 'absent'; 
						@endphp />
					</div>
					@endforeach
					
				</div>
							
			</div> <!-- End of attendance -->

			<button class="btn btn-primary" id="update-attendance">SubmitAttendance</button>

		</div>

		<script type="text/javascript" src="/js/attendance.js" ></script>
	</div>	

	@include('partials.dashboard.school_sidebar')
	
</div>