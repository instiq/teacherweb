<div class="item-content row" id="teacher-status-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane">
						
			<div class="select-status-item" id="availableStatus">
				<img class="status-image" src="/images/icons/free.png" alt="status-image" />
							
				<p class="status">Available</p>
				<input class="select-status-radio" type="radio" name="select-status-radio" aria-label="Radio button" 
				
				@php 
					if(isset($status['availability'])){

						if($status['availability']->status != 'busy'){

							echo 'checked="checked"';

						}
					}else{

						echo 'checked="checked"';

					}

				 @endphp >
			</div>
			<div class="select-status-item" id="busyStatus">
				<img class="status-image" src="/images/icons/busy.png" alt="status-image" />				
				<p class="status">Busy</p>
				<input class="select-status-radio" type="radio" name="select-status-radio" aria-label="Radio button" 
				@php 
					if(isset($status['availability'])){

						if($status['availability']->status == 'busy'){

							echo 'checked="checked"';

						}
					}

				 @endphp >
			</div>

			<button class="btn btn-primary view-child">OK</button>

		</div>

		<script>

			$('.select-status-item').on('click', function(){

				var radio = $(this).find('input[type="radio"]');
				var idAttr = $(this).attr('id');

				if(!$(radio).is(':checked')){
					//We send request to check this option
					showLoading();
					axios.post('/set-availability', {'status' : ((idAttr == 'busyStatus') ? 'busy' : 'available') } )
					.then(function(response){
						if(response.status == 200){
							//alert(response.data);
							location.reload();
							//console.log(response.data);
						}
						else{
							alert(response.data);
						}
					}).catch(function(error){
						alert('An error occured');
					});

				}
			});

		</script>
	</div>
	
	@include('partials.dashboard.school_sidebar')
	
</div>