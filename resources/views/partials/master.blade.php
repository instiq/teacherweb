<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title')</title>

		<link rel="stylesheet" href="/css/app.css" />
		<link rel="stylesheet" href="/css/youngster.css" />
		<link rel="stylesheet" href="/css/animate.css" />
		<link rel="stylesheet" href="/css/fontawesome/all.css" />
		<link rel="stylesheet" href="/css/themefy/themify-icons.css" />

		<script type="text/javascript" src="/js/app.js"></script>
		<script type="text/javascript" src="/js/youngster.js"></script>

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/png" href="/favicon.png" />

		<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
		<script>
		    webshims.setOptions('forms-ext', {types: 'date'});
		webshims.polyfill('forms forms-ext');
		</script>
		
	</head>
	<body >
		<main>
			@yield('navigation')
			<div class="main_content">
				@yield('content')
			</div>
			@yield('footer')
		</main>
		<div class="scroll-top hide"><i class="fas fa-angle-up" style="font-size: 1.5em; line-height: 50px;"></i>
        </div>
	</body>
</html>