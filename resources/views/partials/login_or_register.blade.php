<ul class="auth-links">
   <li class="nav-item">
       <a class="nav-link" href="{{ route('about') }}">{{ __('About') }}</a>
   </li>
@if(! Route::is('login') && ! session()->has('x-auth-token'))
   <li class="nav-item">
       <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
   </li>
@elseif(Route::is('login'))
@else
   <li class="nav-item">
       <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
           {{ __('Logout') }}
       </a>
       <form id="logout-form" action="{{ route('try_logout') }}" method="POST" style="display: none;">
           @csrf
       </form>
   </li>
@endif
</ul>