<div class="modal" id="confirmation-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
        <p>You are about to link <span id="modal_parent_name"></span> to <span id="modal_child_name"></span>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="color: #ddd; width: 120px;">Cancel</button>
        <button type="button" class="btn btn-primary" id="positive" style="float: right; width: 120px;"  >Confirm</button>
        
      </div>
    </div>
  </div>
</div>