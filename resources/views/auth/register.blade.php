@extends('partials.master')

@section('navigation')
@include('partials.main_navigation')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card registration">
                
                <div class="card-body form_holder">

                    <form method="POST" action="{{ route('register') }}"  class="login_form">

                        @csrf

                        <div id="first_form_section" class="form_section">

                            <div class="form-group row">

                                <img src="/images/logo.png" alt="logo image"/>

                            </div>

                            <div class="form-group row">
                                <!-- <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label> -->

                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{ __('Email or phone number') }}">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row separator-row">
                                <div class="separator"></div>
                                <p style="display: inline-block;">OR</p>
                            </div>

                            <div class="form-group row">
                                <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

                                <div class="col-md-12 social-login">
                                    
                                    <input id="email" type="button" value="{{ __('Sign up with Google') }}" class="form-control google-signup" onclick=""/>
                                    <img src="/images/gmail.png" alt="gmail logo" />  

                                </div>
                            </div>

                            <div class="form-group row">
                                <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

                                <div class="col-md-12 social-login">
                                      
                                    <input id="email" type="button" value="{{ __('Sign up with facebook') }}" class="form-control google-signup" onclick=""/>

                                    <img src="/images/facebook.png" alt="facebook logo" />

                                </div>
                            </div>

                            <div class="form-group row justify-content-center" style="margin-top: 60px;" >
                                
                                    <button type="button" class="btn btn-primary" onclick="showNextSection(2);">
                                        {{ __('Continue') }}
                                    </button>
                               
                            </div>


                        </div>



                        <div id="second_form_section" class="form_section">

                            <img src="/images/password.png" alt="password image" />

                            <h6>Create Login details</h6>
                            <small>Your password must consist of at least six characters</small>

                            <div class="col-md-12">
                                <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="email" autofocus placeholder="{{ __('Email or Phone number') }}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12">
                                <label for="name" class="col-md-12 col-form-label">{{ __('Email') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('Email or Phone number') }}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12">
                                <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            

                            <div class="col-md-12">
                                <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Re-enter Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                            

                            <small style="width: 60%; display: block; margin: auto; text-align: center; margin-bottom: 20px; ">By signing up, you agree to the <a href="#"><b>Terms of service</b></a> and <a href="#"><b>privacy policy</b></a></small>

                            <div class="form-group row justify-content-center" style="margin-top: 60px;" >
                                
                                    <button type="button" class="btn btn-primary" onclick="showNextSection(3);">
                                        {{ __('Sign up') }}
                                    </button>
                               
                            </div>



                            

                        </div>

                        <div id="third_form_section" class="form_section">

                            <img src="/images/agreement.png" alt="password image" />

                            <h6>User agreement</h6>
                            <p>By signing up, you agree to Youngster <a href="#">Terms of Service</a><br>
                            to schools registered under this platform<br>
                            and <a href="#">Privacy policy</a>. </p>

                            <p>You also agree to Youngster collecting information from<br>
                                you in order to provide our service. This information<br>
                                includes yoour name, your school your email, geo-location<br>
                                and your phone number <a href="#">Learn more</a></p>


                            <div class="form-group row justify-content-center" style="margin-top: 60px;" >
                            
                                <button type="button" class="btn btn-primary" style="width: 150px;" onclick="goToLogin();">
                                    {{ __('Accept') }}
                                </button>
                               
                            </div>

                            <!-- <div class="form-group row justify-content-center" style="margin-top: 50px; ">
                                <div class="col-md-12" style="text-align: center; ">
                                    <button type="submit" class="btn btn-primary" style="width: 150px;">
                                        {{ __('Accept') }}
                                    </button>
                                </div>
                            </div> -->

                            <a href="#" id="decline">Decline</a>

                        </div>

                       
                    </form>

                </div>

            </div>        
        </div>
    </div>
</div>
@endsection
