@extends('partials.master')

@section('title')

{{ Auth::user() . ' - ' ?? ' - ' }} Dashboard 

@endsection

@section('navigation')
@include('partials.main_navigation')
@endsection

@section('content')
<section class="row dashboard">
	<div class="col-lg-2 col-md-2 dashboard-nav" 
			style="margin: 0px !important; padding: 0px !important;" >
		<div class="dashboard-menu dashboard-header">
			<div class="menu-div">
				<img id="profile-image" src="{{ config('youngster_teacher.image_prefix') . session('img') }}" alt="profile picture" />
				<div id="user-div">
					<p id="username">{!! ucfirst(session('first_name')) !!} {!! ucfirst(session('last_name')) !!}</p>
					<p  id="teaching-status"><img style="width: 10px; margin-top: 0px;" src="/images/icons/red.png" alt="" /> Teaching</p>
				</div>
				
				<!-- <span ><i id="menu-icon" class="fas fa-ellipsis-v"></i></span>
				
				<div class="dashboard-popup">
					<div class="popup-top">
						<div id="popup-profile">
							<img  id="popup-profile-image" src="{{ config('youngster_teacher.image_prefix') . session('img') }}" alt="profile picture" />
						</div>
						<div id="user-details">
							<p id="popup-username">{!! ucfirst(session('first_name')) !!} {!! ucfirst(session('last_name')) !!}</p>
							<p id="popup-email">{!! strtolower(session('email')) !!}</p>
							<a href="#" id="edit-email">Edit email</a>
							<a href="#" id="reset-password">Reset Password</a>
						</div>							
					</div>					
					<div class="popup-bottom">
						<button class="popup-button">Support</button>
						<button class="popup-button">Signout</button>
					</div>
				</div>
				 -->
			</div>
		</div>
		<nav class="dashboard-nav-items">
			<ul id="dashboard-nav-items-list">
				<li class="dashboard-nav-item @if(Route::is('broadcast') || Route::is('dashboard')) active @endif"  data-target="/dashboard/broadcast">
					<img src="/images/icons/notification.png" alt="dashboard icon"/> 
					Broadcast
				</li>
				<li class="dashboard-nav-item @if(Route::is('attendance')) active @endif"  data-target="/dashboard/attendance">
					<img src="/images/icons/attendance.png" alt="dashboard icon"/> 
					Attendance
				</li>
				<li class="dashboard-nav-item @if(Route::is('digital-homework')) active @endif"  data-target="/dashboard/digital-homework">
					<img src="/images/icons/homework.png" alt="dashboard icon"/> 
					Digital Homework
				</li>
				<li class="dashboard-nav-item @if(Route::is('subject-time-table')) active @endif"  data-target="/dashboard/subject-time-table">
					<img src="/images/icons/subject.png" alt="dashboard icon"/> 
					Time Table
				</li>
				<!-- <li class="dashboard-nav-item @if(Route::is('child-location')) active @endif"  data-target="/dashboard/child-location">
					<img src="/images/icons/location.png" alt="dashboard icon"/> 
					
					Child Status
				</li> -->
                <div class="dropdown-divider"></div>

				<li class="dashboard-nav-item @if(Route::is('inbox')) active @endif"  data-target="/dashboard/inbox">
					<img src="/images/icons/inbox.png" alt="dashboard icon"/> 
					Inbox
				</li>		

				<li class="dashboard-nav-item @if(Route::is('chats')) active @endif"   data-target="/dashboard/chats">
					<img src="/images/icons/chat.png" alt="dashboard icon"/> 
					Chats
				</li>

				<li class="dashboard-nav-item @if(Route::is('teacher-status')) active @endif"  data-target="/dashboard/teacher-status">
					<img src="/images/icons/smiley_face.png" alt="dashboard icon"/> 
					Teacher Status
				</li>

			</ul>
		</nav>
	</div>
	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 dashboard-content" style="margin: 0px !important; padding: 0px !important; position: relative;">
		<div class="dashboard-header dashboard-top-header"></div>

		    
		    @if(Route::is('broadcast') || Route::is('dashboard')) 				
				@include('partials.dashboard.broadcast_pane')

		    @elseif(Route::is('attendance'))
				@include('partials.dashboard.attendance_pane')

		    @elseif(Route::is('child-location'))
				@include('partials.dashboard.child_location_pane')	

		    @elseif(Route::is('inbox'))
				@include('partials.dashboard.inbox_pane')

			@elseif(Route::is('teacher-status'))	
				@include('partials.dashboard.teacher_status_pane')

		    @elseif(Route::is('subject-time-table'))
				@include('partials.dashboard.subject_time_table_pane')

		    @elseif(Route::is('digital-homework'))
				@include('partials.dashboard.digital_homework_pane')

		    @elseif(Route::is('chats'))
				@include('partials.dashboard.chats_pane')
			
			@endif
	</div>
	<div class="loading-div">
	<img src="/images/loading.gif" alt="loading image" class="loading" />
	</div>	



</section>
@endsection

@section('footer')
	@include('partials.footer')
@endsection
