<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=> 'loggedin'], function(){

Auth::routes();

});



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', function(){
	return view('about');
})->name('about');


Route::post('try-login', 'Auth\LoginController@tryLogin')->name('login_route');
Route::post('try-logout', 'Auth\LoginController@tryLogout')->name('try_logout');

Route::group(['middleware' => 'check.token'], function(){

	Route::post('/submit-attendance', 'AttendanceController@store')->name('submit-attendance');

	Route::post('/send-assignment', 'AssignmentController@store')->name('send_assignment_route');

	Route::post('/set-availability', 'TeacherStatusController@update')->name('set_availability_route');

	Route::post('/send-chat-message', 'ChatController@sendMessage')->name('send_chat_message');

	Route::post('/get-chat-messages', 'ChatController@getChatMessages')->name('get_chat_messages');

	Route::group(['prefix' => 'dashboard'], function(){

		Route::get('/', 'DashboardController@index')->name('dashboard');
		Route::get('/attendance', 'AttendanceController@index')->name('attendance');
		Route::get('/broadcast', 'DashboardController@index')->name('broadcast');
		Route::get('/chats', 'ChatController@index')->name('chats');
		Route::get('/child-location', 'DashboardController@index')->name('child-location');
		Route::get('/digital-homework', 'AssignmentController@index')->name('digital-homework');
		Route::get('/inbox', 'InboxController@index')->name('inbox');
		Route::get('/select-child', 'DashboardController@index')->name('select-child');
		Route::get('/subject-time-table', 'TimetableController@show')->name('subject-time-table');
		Route::get('/teacher-status', 'TeacherStatusController@index')->name('teacher-status');
		Route::post('/send-message', 'InboxController@create')->name('send_message_route');

	});
});

